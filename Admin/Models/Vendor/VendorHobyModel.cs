﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Admin.Models.Vendor
{
    public class VendorHobyModel
    {
        public string IDHobby { get; set; }
        public string Hobby { get; set; }
    }
}