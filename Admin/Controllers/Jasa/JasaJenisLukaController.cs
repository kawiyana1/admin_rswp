﻿using Admin.Entities.SIM;
using Admin.Helper;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;
using System.Linq.Dynamic;
using Admin.Models.Jasa;

namespace Admin.Controllers.Jasa
{
    public class JasaJenisLukaController : Controller
    {
        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.ADM_ListJenisLuka.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y => y.KodeLuka.Contains(x.Value) || y.Deskripsi.Contains(x.Value));

                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        KodeLuka = x.KodeLuka,
                        Deskripsi = x.Deskripsi,

                    }); ;
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, JasaJenisLukaModel model)
        {
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var userid = User.Identity.GetUserId();
                        var id = 0;
                        if (_process == "CREATE")
                        {
                            id = s.ADM_InsertJenisLuka(model.KodeLuka, model.Deskripsi);
                            s.SaveChanges();
                        }
                        else if (_process == "EDIT")
                        {
                            s.ADM_UpdateJenisLuka(model.KodeLuka, model.Deskripsi);
                            s.SaveChanges();
                        }
                        else if (_process == "DELETE")
                        {
                            s.ADM_DeleteJenisLuka(model.KodeLuka);
                            s.SaveChanges();
                        }

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"SetupJasaJenisLuka-{_process}; id:{id};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
            return HConvert.Success();
        }

        [HttpPost]
        public string Detail(string id)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_ListJenisLuka.FirstOrDefault(x => x.KodeLuka == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            KodeLuka = m.KodeLuka,
                            Deskripsi = m.Deskripsi,



                        }
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion
    }
}