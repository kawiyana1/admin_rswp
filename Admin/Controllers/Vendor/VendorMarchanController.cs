﻿using Admin.Entities.SIM;
using Admin.Helper;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Linq.Dynamic;
using Admin.Models.Vendor;

namespace Admin.Controllers.Vendor
{
    public class VendorMarchanController : Controller
    {
        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.ADM_ListMerchan.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y => y.ID.Contains(x.Value) ||
                            y.NamaBank.Contains(x.Value)||
                                y.Akun_Akun_No.Contains(x.Value) ||
                                y.Akun_Akun_Name.Contains(x.Value)||
                                y.AddCharge_Debet.ToString().Contains(x.Value)||
                                y.AddCharge_Kredit.ToString().Contains(x.Value) ||
                                y.Diskon.ToString().Contains(x.Value));
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        ID = x.ID,
                        NamaBank = x.NamaBank,
                        Akun_Akun_No = x.Akun_Akun_No,
                        Akun_Akun_Name = x.Akun_Akun_Name,
                        AddCharge_Debet = x.AddCharge_Debet,
                        AddCharge_Kredit = x.AddCharge_Kredit,
                        Diskon = x.Diskon,
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, VendorMerchanModel model)
        {
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var userid = User.Identity.GetUserId();
                        if (_process == "CREATE")
                        {
                            s.ADM_InsertMerchan(model.ID, model.NamaBank,model.Akun_ID,model.AddCharge_Debet, model.AddCharge_Kredit, model.Diskon);
                            s.SaveChanges();
                        }
                        else if (_process == "EDIT")
                        {
                            s.ADM_UpdateMerchan(model.ID, model.NamaBank, model.Akun_ID, model.AddCharge_Debet, model.AddCharge_Kredit, model.Diskon);
                            s.SaveChanges();
                        }
                        else if (_process == "DELETE")
                        {
                            s.ADM_DeleteMerchan(model.ID);
                            s.SaveChanges();
                        }

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"SetupMerchan-{_process}; id:{model.ID};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
            return HConvert.Success();
        }

        [HttpPost]
        public string Detail(string id)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_ListMerchan.FirstOrDefault(x => x.ID == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            ID = m.ID,
                            NamaBank = m.NamaBank,
                            Akun_ID = m.Akun_ID,
                            Akun_Akun_No = m.Akun_Akun_No,
                            Akun_Akun_Name = m.Akun_Akun_Name,
                            AddCharge_Debet = m.AddCharge_Debet,
                            AddCharge_Kredit = m.AddCharge_Kredit,
                            Diskon = m.Diskon,
                        }
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== L O O K U P - R E K E N I N G

        [HttpPost]
        public string ListLookupRekening(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.ADM_GetRekKategoriVendor.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y =>
                                y.Akun_No.Contains(x.Value) ||
                                y.Akun_Name.Contains(x.Value));
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        Akun_ID = x.Akun_ID,
                        Akun_No = x.Akun_No,
                        Akun_Name = x.Akun_Name,
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion
    }
}