﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Admin.Models.Jasa
{
    public class JasaItemKomponenJasaModel
    {
        
            public string KomponenBiayaID { get; set; }
            public string KomponenName { get; set; }
            public string KelompokAkun { get; set; }
            public string PostinganKe { get; set; }
            public string KelompokJasa { get; set; }
            public int AkunNoHPP { get; set; }
            public int AkunNoLawanHPP { get; set; }
            public bool IncludeInsentif { get; set; }
            public bool ExcludeCostRS { get; set; }
            public string HutangKe { get; set; }

    }
}