﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace Admin.Models.Other
{
    public class OtherSectionModel
    {
        public string Id { get; set; }
        public string Nama { get; set; }
        public string PenanggunJawab { get; set; }
        public string TipePelayanan { get; set; }
        public string KelompokSection { get; set; }
        public bool RIOnly { get; set; }
        public bool RuangBayi { get; set; }
        public bool ICU { get; set; }
        public string Poliklinik { get; set; }
        public string KodeNoBukti { get; set; }
        public int? JumlahTT { get; set; }
        public string UnitBisnis { get; set; }
        public string IPFarmasi { get; set; }
        public string Pelayanan { get; set; }
        public bool BlokCoPayment { get; set; }
        public bool Aktif { get; set; }
        public int? Customer { get; set; }
        public int? AkunMutasiIn { get; set; }
        public int? AkunMutasiOut { get; set; }
    }
}