﻿using Admin.Entities.SIM;
using Admin.Helper;
using Admin.Models.Jasa;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Admin.Controllers.Jasa
{
    [Authorize(Roles = "Admin")]
    public class JasaDetailMCUPaketController : Controller
    {
        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.ADM_ListPaketMCU.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.JasaID.Contains(x.Value) ||
                                y.JasaName.Contains(x.Value));
                        }
                        else if (x.Key == "Jenis" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y => y.Ket == x.Value);
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Id = m.JasaID,
                        Nama = m.JasaName,
                        Ket = m.Ket
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, JasaDetailMCUPaketModel model)
        {
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var userid = User.Identity.GetUserId();
                        string id = "";
                        if (_process == "EDIT")
                        {
                            if (model.Detail == null) model.Detail = new List<JasaDetailMCUPaketDetailModel>();

                            var d = s.ADM_GetDetailPaketMCU.Where(x => x.MCUID == model.Id).ToList();
                            foreach (var x in d)
                            {
                                var _d = model.Detail.FirstOrDefault(y => y.Id == x.JasaID);
                                //delete
                                if (_d == null) s.ADM_DeletePaketMCU(model.Id, x.JasaID);
                            }
                            foreach (var x in model.Detail)
                            {
                                var _d = d.FirstOrDefault(y => y.JasaID == x.Id);
                                if (_d == null)
                                {
                                    // new
                                    s.ADM_InsertPaketMCU(
                                        model.Id,
                                        x.Id,
                                        x.Section,
                                        x.Kelompok ?? "",
                                        x.Qty,
                                        x.Harga,
                                        x.HargaHC
                                    );
                                    foreach (var y in x.Detail) 
                                    {
                                        s.ADM_InsertDetailPaketMCU(
                                            model.Id,
                                            x.Id,
                                            y.Id,
                                            y.Harga,
                                            y.HargaHC
                                        );
                                    }
                                }
                                else
                                {
                                    // edit
                                    s.ADM_UpdateDetailPaketMCU(
                                        model.Id,
                                        x.Id,
                                        x.Section,
                                        x.Qty,
                                        x.Harga,
                                        x.HargaHC,
                                        x.Kelompok ?? ""
                                    );
                                    foreach (var y in x.Detail)
                                    {
                                        s.ADM_UpdateJasaPaketMCU(
                                            model.Id,
                                            x.Id,
                                            y.Id,
                                            y.Harga,
                                            y.HargaHC
                                        );
                                    }
                                }

                            }
                            s.SaveChanges();
                        }

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"JasaDetailMCUPaket-{_process}; id:{id};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();

                        return JsonConvert.SerializeObject(new { IsSuccess = true, Id = id });
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
        }

        [HttpPost]
        public string Detail(string id)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_GetPaketMCU.FirstOrDefault(x => x.MCUID == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    var d = s.ADM_GetDetailPaketMCU.Where(x => x.MCUID == m.MCUID).ToList();
                    var dd = s.ADM_GetJasaPaketMCU.Where(x => x.MCUID == m.MCUID).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            Id = m.MCUID,
                            Nama = m.Deskripsi,
                            m.TotalHarga,
                            m.TotalHargaHC
                        },
                        Detail = d.ConvertAll(x => new
                        {
                            Id = x.JasaID,
                            Nama = x.Deskripsi,
                            SectionId = x.SectionID,
                            SectionNama = x.SectionName,
                            Qty = x.Qty,
                            Harga = x.Harga,
                            HargaHC = x.HargaHC,
                            Jumlah = x.Jumlah,
                            Kelompok = x.Kelompok
                        }),
                        DetailDetail = dd.ConvertAll(x => new
                        {
                            JasaId = x.JasaID,
                            Id = x.KomponenID,
                            Nama = x.KomponenName,
                            Harga = x.Harga,
                            HargaHC = x.HargaHC
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== L O O K U P

        [HttpPost]
        public string ListLookupJasa(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.ADM_ListJasa.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y =>
                                y.JasaID.Contains(x.Value) ||
                                y.JasaName.Contains(x.Value));
                        else if (x.Key == "Kategori" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y => y.KategoriBiaya_KategoriJasaID == x.Value);
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        Id = x.JasaID,
                        Nama = x.JasaName,
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        [HttpPost]
        public string GetJasa(string id)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_GetJasaBaru.Where(x => x.JasaID == id).FirstOrDefault();
                    var d = s.ADM_GetKomponenBaru.Where(x => x.JasaID == id).ToList();

                    var r = new {
                        Id = m.JasaID,
                        Nama = m.Deskripsi,
                        SectionId = m.SectionID,
                        SectionNama = m.SectionName,
                        Qty = m.Qty,
                        Harga = m.Harga,
                        HargaHC = m.HargaHC,
                        Jumlah = m.Jumlah,
                        Kelompok = m.Kelompok,
                        Detail = d.ConvertAll(x => new {
                            JasaId = x.JasaID,
                            Id = x.KomponenBiayaID,
                            Nama = x.KomponenName,
                            Harga = x.Harga,
                            HargaHC = x.HargaHC
                        })
                    };
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion
    }
}